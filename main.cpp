/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: certi
 *
 * Created on 24 de Janeiro de 2018, 08:20
 */

#include <cstdlib>
#include <iostream>
#include "src/Speaker.h"

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    Speaker speaker ("World!");
    cout << speaker.message() << endl;
    return 0;
}

