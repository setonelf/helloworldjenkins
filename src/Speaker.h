/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Speaker.h
 * Author: certi
 *
 * Created on 24 de Janeiro de 2018, 08:59
 */

#ifndef SPEAKER_H
#define SPEAKER_H

#include <iostream>

using namespace std;

class Speaker {
public:
    Speaker();
    Speaker(const Speaker& orig);
    Speaker(const string& who);
    virtual ~Speaker();
    string message() const;
private:
    string who;

};

#endif /* SPEAKER_H */

