/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   HelloWorldCppUnitTest.cpp
 * Author: certi
 *
 * Created on 24/01/2018, 09:16:46
 */

#include "HelloWorldCppUnitTest.h"
#include "Speaker.h"


CPPUNIT_TEST_SUITE_REGISTRATION(HelloWorldCppUnitTest);

HelloWorldCppUnitTest::HelloWorldCppUnitTest() {
}

HelloWorldCppUnitTest::~HelloWorldCppUnitTest() {
}

void HelloWorldCppUnitTest::setUp() {
}

void HelloWorldCppUnitTest::tearDown() {
}

void HelloWorldCppUnitTest::testMessage() {
    Speaker speaker ("Google!");
    string result = speaker.message();
    if (true /*check result*/) {
        CPPUNIT_ASSERT(result == "Hello Google!");
    }
}

void HelloWorldCppUnitTest::testMessageNobody() {
    Speaker speaker;
    string result = speaker.message();
    if (true /*check result*/) {
        CPPUNIT_ASSERT(result == "Hello ");
    }
}

